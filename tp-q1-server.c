// STANDARD
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// SHD MEMORY
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

// HANDLE SIGNAL
#include <signal.h>

// HANDLE THREADS
#include <pthread.h>

// SEMAPHORE
#include <sys/sem.h>

#define THREADS 10
#define IPC_KEY 25
#define SEM_KEY 5

typedef struct Database {
	int Artus;
	int Fary;
	int Haroun;
} Shows;

typedef struct Calls {
	char action[20];
	char name[20];
    int ack;
    int nack;
    int number;
    int response;
} Call;

Call call;
Call *p_call = &call;
Shows shows = {100, 100, 100};
int NTHREADS = 10;
int SHARED_MEMORY_ID;
int SEM_ID;
pthread_t threads_id[THREADS];
struct sembuf operation;



void welcome_message(){
    printf("===================================\n");
    printf("==========CTRL+C to EXIT===========\n");
    printf("\n");
}


void init_semaphore() {
    /* Initialize semaphore to 1 */
    SEM_ID = semget(SEM_KEY, 1, IPC_CREAT | IPC_EXCL | 0600);
    semctl(SEM_ID, 0, SETVAL, 1);
}


void kill_semaphore() {
    semctl(SEM_ID, 0, IPC_RMID, 0);
}


void join_threads() {
    for (int i = 0; i < THREADS; i++) {
        pthread_join(threads_id[i], NULL);
        NTHREADS ++;
    }
}


void close_connection() {
    // WAITINGS THREADS BEFORE CLOSING.
    join_threads();
    // KILL SEMAPHORE
    kill_semaphore();
	// DETACHING SHARED MEMORY
	shmdt(p_call);
	// DELETING SHARED MEMORY
	shmctl(SHARED_MEMORY_ID, IPC_RMID, NULL);

    printf("Connection closed.");
	exit(0);
}


void error(char *message) {
	printf("[Error] %s\n", message);
    close_connection();
	exit(1);
}


void sem_operation(char *type) {
    operation.sem_num =0;
    operation.sem_flg = 0;

    if(strcmp(type, "P") == 0){
        operation.sem_op = -1;
        semop(SEM_ID, &operation, 1);
    } else if (strcmp(type, "V") == 0){
        operation.sem_op = 1;
        semop(SEM_ID, &operation, 1);

    } else {
        error("This operation is not allowed.");
    }
}


void init_shared_memory() {
    // REQUEST SHARED MEMORY
    SHARED_MEMORY_ID = shmget((key_t)IPC_KEY, sizeof(call), 0666 | IPC_CREAT | IPC_EXCL);
    printf("%i", SHARED_MEMORY_ID);

    if (SHARED_MEMORY_ID < 0) {
		error("Shared memory can not be shared.");
	}

    // ATTACHED SHARED MEMORY
    if ((p_call = shmat(SHARED_MEMORY_ID, NULL, 0)) == (Call *) -1) {
		error("Shared memory can not be attached.");
	}
}


void * check_tickets_available() {
    char *name = p_call->name;
    p_call->ack = 0;
    p_call->nack = 0;

    // Check the show availability
    // Write the response into our structure.
    int result;

    if (strcmp(name, "fary") == 0) {
        result = shows.Fary;
    } else if (strcmp(name, "artus") == 0) {
        result = shows.Artus;
    } else if (strcmp(name, "haroun") == 0) {
        result = shows.Haroun;
    }
    p_call->response = result;
    p_call->ack = 1;
}


void * make_reservation() {
    int number = p_call->number;
    char *name = p_call->name;
    p_call->ack = 0;
    p_call->nack = 0;

    // We ask permission to continue our execution and grab our mutex.
    sem_operation("P");

    // Check if minus reservation there is enought tickets to sell.
    // If not, switch nack to 1, else continue.
    check_tickets_available();
    if((p_call->response - number) < 0) {
        p_call->ack = 0;
        p_call->nack = 1;
        name = "none";
    }

    if (strcmp(name, "fary") == 0) {
        shows.Fary -= number;
        p_call->response = shows.Fary;
    } else if (strcmp(name, "artus") == 0) {
        shows.Artus -= number;
        p_call->response = shows.Artus;
    } else if (strcmp(name, "haroun") == 0) {
        shows.Haroun -= number;
        p_call->response = shows.Haroun;
    }

    if (p_call->nack == 1){
        printf("Reservation not completed\n");
    } else {
        p_call->ack = 1;
        printf("Reservation done for %i tickets.\n", p_call->number);
        printf("Now %i tickets are available\n", p_call->response);
    }

    // We put our mutex back in the queue, to the next processus.
    sem_operation("V");
}


void create_read_thread() {
    if (NTHREADS > 0) {
        // We create our thread.
        pthread_create(&threads_id[NTHREADS], NULL, check_tickets_available, NULL);
        NTHREADS -= 1;
    } else {
        // If none is available, we play join_threads to liberate.
        join_threads();
        create_read_thread();
    }
}


void create_reservation_thread() {
    if (NTHREADS > 0) {
        // We create our thread.
        pthread_create(&threads_id[NTHREADS], NULL, make_reservation, NULL);
        NTHREADS -= 1;
    } else {
        // If none is available, we play join_threads to liberate.
        join_threads();
        create_read_thread();
    }
}


int main() {

	welcome_message();
	init_shared_memory();
    init_semaphore();

	// Handle CTRL+C to EXIT server.
	signal(SIGINT, close_connection);

	// Running server.
    do {
        printf("call: %s\n", p_call->action);

        if (strcmp(p_call->action, "read") == 0) {
            create_read_thread();
        } else if (strcmp(p_call->action, "reservation") == 0) {
            create_reservation_thread();
        }
    } while (1);

    close_connection();
	return 0;
}
