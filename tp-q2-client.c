// Standard
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// For socket APIs
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// Structure for storing address information
#include <netinet/in.h>

#define PORT 12345

struct sockaddr_in server_address;
int SOCK_FD;
int CLIENT_FD;

typedef struct Calls {
	char action[20];
	char name[20];
    int number;
    int response;
	int error;
} Call;

Call call;
Call *p_call = &call;


void error(char *message) {
	printf("[Error] %s\n", message);
	exit(1);
}


void init_socket_connection() {
	// Connect informations to existing socket.
	SOCK_FD = socket(AF_INET, SOCK_STREAM, 0);

    server_address.sin_family = AF_INET;
    server_address.sin_port = PORT;
    server_address.sin_addr.s_addr = INADDR_ANY;

	// Client ask connection to our server.
	CLIENT_FD = connect(SOCK_FD, (struct sockaddr*)&server_address, sizeof(server_address));

    if (CLIENT_FD < 0) {
        error("Connection Failed\n");
    }
}

int main(int argc, char *argv[]) {
	char response[1024];
	init_socket_connection();

	p_call->number = 0;
	strcpy(p_call->action, argv[1]);
	strcpy(p_call->name, argv[2]);

	// must be optional, only for reservation command.
	if (argv[3]){
		p_call->number = atoi(argv[3]);
	}

	// Send message to server.
	send(SOCK_FD, p_call, sizeof(call), 0);

	// Receive message from server.
	recv(SOCK_FD, response, 1024, 0);
	printf("%s\n", response);

	// Closing connected socket.
    close(CLIENT_FD);
    return 0;
}
