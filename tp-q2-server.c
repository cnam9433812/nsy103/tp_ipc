// Standard
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// For socket APIs
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// Structure for storing address information
#include <netinet/in.h>

// Handle signals
#include <signal.h>

#define PORT 12345

typedef struct Database {
	int Artus;
	int Fary;
	int Haroun;
} Shows;

typedef struct Calls {
	char action[20];
	char name[20];
    int number;
    int response;
    int error;
} Call;

Call call;
Call *p_call = &call;
Shows shows = {100, 100, 100};

struct sockaddr_in server_address;
int SOCK_FD;
int SOCK_NEW;
pid_t NUMPID;


void welcome_message(){
    printf("===================================\n");
    printf("==========CTRL+C to EXIT===========\n");
    printf("\n");
}


void close_connection() {
    // Closing socket
    close(SOCK_NEW);
    close(SOCK_FD);

    printf("Connection closed.");
	exit(0);
}


void error(char *message) {
	printf("[Error] %s\n", message);
    close_connection();
	exit(1);
}


void init_socket_connection() {
    // Create socket TCP with ipv4
	SOCK_FD = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    server_address.sin_family = AF_INET;
    server_address.sin_port = PORT;
    server_address.sin_addr.s_addr = INADDR_ANY;

    // Bind and listen on our created socket.
    bind(SOCK_FD, (struct sockaddr*)&server_address, sizeof(server_address));
	listen(SOCK_FD, 1);
}


void check_tickets_available() {
    char *name = p_call->name;

    // Check the show availability
    // Write the response into our structure.
    int result;

    if (strcmp(name, "fary") == 0) {
        result = shows.Fary;
    } else if (strcmp(name, "artus") == 0) {
        result = shows.Artus;
    } else if (strcmp(name, "haroun") == 0) {
        result = shows.Haroun;
    }
    p_call->response = result;
}


void make_reservation() {
    int number = p_call->number;
    char *name = p_call->name;

    // Check if minus reservation there is enought tickets to sell.
    // If not, switch error to 1, else continue.
    check_tickets_available();
    if((p_call->response - number) < 0) {
        name = "none";
        p_call->error = 1;
    }

    if (strcmp(name, "fary") == 0) {
        shows.Fary -= number;
        p_call->response = shows.Fary;
    } else if (strcmp(name, "artus") == 0) {
        shows.Artus -= number;
        p_call->response = shows.Artus;
    } else if (strcmp(name, "haroun") == 0) {
        shows.Haroun -= number;
        p_call->response = shows.Haroun;
    }

    if (p_call->error == 1){
        printf("[log] Reservation not completed\n");
    } else {
        printf("[log] Reservation done for %i tickets.\n", p_call->number);
        printf("[log] Now %i tickets are available\n", p_call->response);
    }
}

void send_message(char *message){
    // Sending response to client.
    send(SOCK_NEW, message, 1024, 0);
}

int main() {
    char result[10];
    char message[1024];
	welcome_message();
    init_socket_connection();

	// Handle CTRL+C to EXIT server.
	signal(SIGINT, close_connection);
    // Handle zombies
	signal(SIGCHLD, SIG_IGN);

    while (1) {
        // Accept connection with client.
        SOCK_NEW = accept(SOCK_FD, NULL, NULL);

        // Handle requests from client.
        recv(SOCK_NEW, p_call, sizeof(call), 0);
        printf("[log] action: %s, name: %s\n", p_call->action, p_call->name);


        if (strcmp(p_call->action, "read") == 0) {
            /* READ; Check in Database structure how many tickets left. */
            check_tickets_available();

            // Convert int to str.
            sprintf(result, "%d", p_call->response);
            // Then combining several variables into one message.
            snprintf(
                message,
                sizeof(message),
                "For %s, %s tickets are still available.",
                p_call->name,
                result
            );
            // Send this message to our client as a response.
            send_message(message);
        } else if (strcmp(p_call->action, "reservation") == 0) {
            /* RESERVATION; Check in Database structure how many tickets left.
             * If this is possible to make a reservation, we return a confirmation to
             * the client. Otherwise, if its not, we return an error.
             */

            // Create child process just for reservation.
            NUMPID = fork();
            if (NUMPID == 0){
                close(SOCK_FD);
                make_reservation();

                if (p_call->error == 1){
                    // Convert int to str.
                    sprintf(result, "%d", p_call->response);
                    // Then combining several variables into one message.
                    snprintf(
                        message,
                        sizeof(message),
                        "There is not enought tickets for %s. %s tickets left.",
                        p_call->name,
                        result
                    );
                    send_message(message);

                    close(SOCK_NEW);
                    exit(0);
                } else {
                    // Convert int to str.
                    sprintf(result, "%d", p_call->number);
                    // Then combining several variables into one message.
                    snprintf(
                        message,
                        sizeof(message),
                        "You just booked %s tickets for %s.",
                        result,
                        p_call->name
                    );
                    // Send this message to our client as a response.
                    send_message(message);

                    close(SOCK_NEW);
                    exit(0);
                }
            } else {
                close(SOCK_NEW);
            }
        }
    }

    close_connection();
	return 0;
}
