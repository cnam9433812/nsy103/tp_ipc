/*
    How to use the program:
    ./client COMMAND NAME_SHOW
    ex: ./client read Fary

    COMMANDS:
        - Read
        - Reservation

    SHOWS:
        - Fary
        - Artus
        - Alban_Ivanov
*/

// STANDARD
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// SHD MEMORY
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

#define KEY 25


typedef struct Calls {
	char action[20];
	char name[20];
    int ack;
    int nack;
	int number;
    int response;
} Call;

Call call;
Call *p_call = &call;


void close_connection() {
	// init struct to avoid to keep going a reservation and reading on server.
	strcpy(p_call->action, "none");
	// DETACHING SHARED MEMORY
	shmdt(p_call);
}


void error(char *message) {
	printf("[Error] %s\n", message);
    close_connection();
	exit(1);
}


void init_shared_memory() {
    int shared_memory_id;

    // REQUEST SHARED MEMORY ALREADY CREATED BY SERVER.
    shared_memory_id = shmget((key_t)KEY, sizeof(call), 0);

    if (shared_memory_id < 0) {
		error("Shared memory can not be shared.");
	}

    // ATTACHED SHARED MEMORY
    if ((p_call = shmat(shared_memory_id, NULL, 0)) == (Call *) -1) {
		error("Shared memory can not be attached.");
	}
}

void handle_response() {
	if (strcmp(p_call->action, "read") == 0){
		printf("There is %i tickets left for %s.\n", p_call->response, p_call->name);
	} else if (strcmp(p_call->action, "reservation") == 0) {
		printf("There is %i tickets left for %s.\n", p_call->response, p_call->name);
		printf("You made a reservation for %i tickets.\n", p_call->number);
	}

}


int main(int argc, char *argv[]) {

    /* INIT */
    init_shared_memory();

    /* INTEL */
	p_call->number = 0;
	strcpy(p_call->action, argv[1]);
	strcpy(p_call->name, argv[2]);

	// must be optional, only for reservation command.
	if (argv[3]){
		p_call->number = atoi(argv[3]);
	}
	printf("action: %s, name: %s, number: %i\n", p_call->action, p_call->name, p_call->number);


	do {
		// We are waiting the answer from the server.
        if (p_call->ack == 1) {
			handle_response();
			break;
        } else if (p_call->nack == 1) {
			error("Sorry, the reservation is not possible for this show.");
        }
    } while (1);

    close_connection();
    return 0;
}
